
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var async = require('async');
var crypto = require('crypto');

var User = require('../models/user');
var secret = require('../secret/secret');

module.exports = (app, passport) =>{

    app.get('/', (req, res, next) =>{
        if(req.session.cookie.originalMaxAge != null){
            res.redirect('/home');
        }
        else {
            res.render('index', {title: 'Index || Node Express'});
        }
    });

    app.get('/signup', (req, res) => {
        var errors = req.flash('error');
        res.render('user/signup', {title: 'Sign Up || Node Express', messages: errors, hasErrors: errors.length > 0});
    });

    app.post('/signup', validate, passport.authenticate('local.signup', {
        successRedirect: '/home',
        failureRedirect: '/signup',
        failureFlash: true
    }));

    app.get('/login', (req, res) => {
        var errors = req.flash('error');
        res.render('user/login', {title: 'Login || Node Express', errors: errors, hasErrors: errors.length > 0});
    });

    app.post('/login', loginValidation, passport.authenticate('local.login', {
        // successRedirect: '/home',
        failureRedirect: '/login',
        failureFlash: true
    }), (req, res) => {
        if(req.body.rememberme){
            req.session.cookie.maxAge = 30*24*60*60*1000 // 30 days
        }
        else {
            req.session.cookie.expires = null
        }
        res.redirect('/home');
    });

    app.get('/home', (req, res) => {
        var errors = req.flash('error');
        res.render('home', {title: 'Home || Node Express', user: req.user});
    });

    app.get('/forgot', (req, res) => {
        var errors = req.flash('error');
        var info = req.flash('info');
        res.render('user/forgot', {title: 'Forgot password', messages: errors, hasErrors: errors.length > 0, info: info, noErrors: info.length > 0});
    });

    app.post('/forgot', (req, res, next) => {
        async.waterfall([
            function (callback) {
                crypto.randomBytes(20, (err, buff) => {
                   var rand = buff.toString('hex');
                   callback(err, rand);
                });
            },

            function (rand, callback) {
                User.findOne({'email': req.body.email}, (err, user) => {
                    if(!user){
                        req.flash('error', 'User with this email is not exist or email is invalid.');
                        return res.redirect('/forgot');
                    }

                    user.passwordResetToken = rand;
                    user.passwordResetExpires = Date.now() + 60*60*1000;

                    user.save((err) => {
                        callback(err, rand, user)
                    });
                })
            },

            function (rand, user, callback) {
                var smtpTransport = nodemailer.createTransport({
                   service: 'Gmail',
                    auth: {
                       user: secret.auth.user,
                        pass: secret.auth.pass
                    }
                });

                var mailOptions = {
                    to: user.email,
                    from: 'Node Mongo Express ' + '<' + secret.auth.user + '>',
                    subject: 'Application password reset token',
                    text: 'You have requested for password reset token. \n\n' +
                        'Please click on the link below to complete the process: \n\n' +
                        'http://localhost:3001/reset/' + rand + '\n\n'
                };

                smtpTransport.sendMail(mailOptions, (err, response) => {
                    req.flash('info', 'A password reset token has been sent to ' + user.email);
                    return callback(err, user);
                });
            }
        ], (err) => {
            if(err){
                return next(err);
            }
            res.redirect('/forgot');
        })
    });

    app.get('/reset/:token', (req, res) => {
        User.findOne({passwordResetToken: req.params.token, passwordResetExpires: {$gt: Date.now()}}, (err, user) => {
            if (!user){
                req.flash('error', 'Password reset token has expired or is invalid. Enter your email to get new token.');
                return res.redirect('/forgot');
            }
            var errors = req.flash('error');
            var success = req.flash('success');
            res.render('user/reset', {title: 'Reset your password', messages: errors, hasErrors: errors.length > 0, success: success, noErrors: success.length > 0});
        });
    });

    app.post('/reset/:token', (req, res) => {
        async.waterfall([
            function (callback) {
                User.findOne({passwordResetToken: req.params.token, passwordResetExpires: {$gt: Date.now()}}, (err, user) => {
                    if (!user){
                        req.flash('error', 'Password reset token has expired or is invalid. Enter your email to get new token.');
                        return res.redirect('/forgot');
                    }

                    req.checkBody('password', 'Password is Required').notEmpty();
                    req.checkBody('password', 'Password must be less than 5').isLength({min: 5});
                    req.checkBody('password', 'Password must contain at least one number').matches(/^(?=.*\d)(?=.*[a-z])[0-9a-z]{5,}$/, "i");

                    var errors = req.validationErrors();
                    if(req.body.password == req.body.cpassword){
                        if (errors){
                            var messages = [];
                            errors.forEach((error) => {
                                messages.push(error.msg);
                            });
                            req.flash('error', messages);
                            res.redirect('/reset/' + req.params.token);
                        }
                        else {
                            user.password = user.encryptPassword(req.body.password);
                            user.passwordResetToken = undefined;
                            user.passwordResetExpires = undefined;

                            user.save((err) => {
                                req.flash('success', 'Your password has been successfully updated.');
                                callback(err, user);
                            })
                        }
                    }
                    else {
                        req.flash('error', 'Password and Confirm password field are not equal.');
                        res.redirect('/reset/' + req.params.token);
                    }
                });
            },
            function (user, callback) {
                var smtpTransport = nodemailer.createTransport({
                    service: 'Gmail',
                    auth: {
                        user: secret.auth.user,
                        pass: secret.auth.pass
                    }
                });

                var mailOptions = {
                    to: user.email,
                    from: 'Node Mongo Express ' + '<' + secret.auth.user + '>',
                    subject: 'Your password has been updated.',
                    text: 'This is a confirmation that you updated your password for email: ' + user.email
                };

                smtpTransport.sendMail(mailOptions, (err, response) => {
                    callback(err, user);
                    var error = req.flash('error');
                    var success = req.flash('success');
                    res.render('user/reset', {title: 'Reset your password', messages: error, hasErrors: error.length > 0, success: success, noErrors: success.length > 0});
                });
            }
        ])
    });

    app.get('/logout', (req, res) =>{
       req.logout();
       req.session.destroy((err) =>{
           res.redirect('/');
       })
    });
}


function validate(req, res, next) {
    req.checkBody('fullname', 'Fullname is Required').notEmpty();
    req.checkBody('fullname', 'Fullname must be less than 5').isLength({min: 5});
    req.checkBody('email', 'Email is Required').notEmpty();
    req.checkBody('email', 'Email is Invalid').isEmail();
    req.checkBody('password', 'Password is Required').notEmpty();
    req.checkBody('password', 'Password must be less than 5').isLength({min: 5});
    req.checkBody('password', 'Password must contain at least one number').matches(/^(?=.*\d)(?=.*[a-z])[0-9a-z]{5,}$/, "i");

    var errors = req.validationErrors();
    if(errors){
        var messages = [];
        errors.forEach((error) => {
            messages.push(error.msg);
        });
        req.flash('error', messages);
        res.redirect('/signup');
    }
    else{
        return next();
    }
}

function loginValidation(req, res, next) {
    req.checkBody('email', 'Email is Required').notEmpty();
    req.checkBody('email', 'Email is Invalid').isEmail();
    req.checkBody('password', 'Password is Required').notEmpty();
    req.checkBody('password', 'Password must be less than 5').isLength({min: 5});
    req.checkBody('password', 'Password must contain at least one number').matches(/^(?=.*\d)(?=.*[a-z])[0-9a-z]{5,}$/, "i");

    var loginErrors = req.validationErrors();
    if(loginErrors){
        var messages = [];
        loginErrors.forEach((error) => {
            messages.push(error.msg);
        });
        req.flash('error', messages);
        res.redirect('/login');
    }
    else{
        return next();
    }
}