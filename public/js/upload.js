$(document).ready(function () {
    $('.upload-btn').on('click', function () {
        $('#upload-input').click();
        $('.progress-bar').text('0%');
        $('.progress-bar').width('0%');
    });

    // document.getElementById('upload-input').onchange = function () {
    //     upload();
    // };
    // function upload() {
    //     var upload = document.getElementById('upload-input');
    //     var image = upload.files[0];
    //     alert(image);
    //     $.ajax({
    //         url:"/upload",
    //         type: "POST",
    //         data: new FormData($('#form')[0]),
    //         contentType:false,
    //         cache: false,
    //         processData:false,
    //         success:function (msg) {}
    //     });
    // };



    $('#upload-input').on('change', function () {
        var uploadInput = $('#upload-input').val().split('\\').pop();
        if(uploadInput != ''){
            var formData = new FormData($('#form')[0]);
            formData.append('upload', uploadInput);

            $.ajax({
                url: '/upload',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    // uploadInput.val('');
                },

                xhr: function () {
                    var xhr = new XMLHttpRequest();
                    xhr.upload.addEventListener('progress', function (e) {
                        if (e.lengthComputable){
                            var uploadPercent = e.loaded / e.total;
                            uploadPercent = (uploadPercent * 100);
                            $('.progress-bar').text(uploadPercent + '%');
                            $('.progress-bar').width(uploadPercent + '%');

                            if (uploadPercent === 100){
                                $('.progress-bar').text('Done');
                                $('#completed').text('File uploaded');
                            }
                        }
                    }, false);

                    return xhr;
                }
            })
        }
    });
});